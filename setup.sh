#!/usr/bin/env bash
set -Eeuo pipefail
trap do_error ERR SIGTERM

_PWD=$(pwd)

srouce common.sh
setup_tmp
SETUP_SCRIPT="$_TMP_PATH/npm_setup_repo.sh"

run_setup(){
  if [ "$(uname)" != "Linux" ]; then
    warn "OS NOT SUPPORTED"
    exit 1
  fi

  DISTRO=$_DISTRO_ID
  if [ "$DISTRO" != "alpine" ] && [ "$DISTRO" != "ubuntu" ] && [ "$DISTRO" != "debian" ]; then
    warn "DISTRO NOT SUPPORTED"
    exit 1
  fi

  if [ "$DISTRO" = "ubuntu" ]; then
    DISTRO="debian"
  fi

  wget -O "$SETUP_SCRIPT" "$URL/$DISTRO/distro.sh"
  chmod +x "$SETUP_SCRIPT"

  cd "$_ROOT_PATH"
  bash "$SETUP_SCRIPT"
  bash "$_ROOT_PATH/setup/$DISTRO/install.sh"
  cd "$_PWD"
}

run_setup > >(tee -a "$_LOG_FILE") 2> >(tee -a "$_LOG_FILE" >&2)
