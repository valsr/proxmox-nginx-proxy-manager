#!/usr/bin/env bash
# shellcheck disable=SC2034
#
# Common/shared functons and variables accross scripts
#

info() {
  printf "\e[32m[info] %b\e[0m\n" "$*"
}

warn() {
  printf "\e[33m[warn] %b\e[0m\n" "$*"
}

error() {
  printf "\e[31m[error] %b\e[0m\n" "$*"
}

do_error() {
  error "Script error occurred at line $LINENO. Offending code:\n\n  $BASH_COMMAND\n\nPlease review the logs (${_LOG_FILE:="tmp/log"}), correct the issue and run the installer again"

  cd "$_PWD" || exit 3
  exit 1
}

_ROOT_PATH=/opt/proxmox-nginx-proxy-manager
_TMP_DIR=tmp
_TMP_PATH="${_ROOT_PATH}/${_TMP_DIR}"
_PATCH_DIR=patches
_NPM_DIR=npm
_CONFIG_FILE=".pnpm"
_LOG_FILE="$_TMP_PATH/log"
_REPO_URL=https://gitlab.com/valsr/proxmox-nginx-proxy-manager/-/raw/main/setup/
_NPMURL="https://github.com/NginxProxyManager/nginx-proxy-manager"
_NPM_REPO="https://github.com/NginxProxyManager/nginx-proxy-manager.git"
_DISTRO_ID=$(cat /etc/*-release | grep -w ID | cut -d= -f2 | tr -d '"')
_DISTRO_VERSION=$(grep -w VERSION_ID /etc/*-release | cut -d= -f2 | tr -d '"')

if [ "$_DISTRO_ID" == "alpine" ]; then
  _DISTRO_CODENAME=""
  _DISTRO_MAJOR_VERSION=$(echo "$_DISTRO_VERSION" | cut -d. -f 1,2)
else
  _DISTRO_CODENAME=$(cat /etc/*-release | grep -w VERSION_CODENAME | cut -d= -f2 | tr -d '"') # not available on alpine
  _DISTRO_MAJOR_VERSION=$(echo "$_DISTRO_VERSION" | cut -d. -f 1)
fi

CONFIG_PNPM_KEY="PNPM_VERSION"
CONFIG_NPM_KEY="NPM_VERSION"
declare -A CONFIG=()

# Handsle configuration saving
save_config() {
  config="$_ROOT_PATH/$_CONFIG_FILE"

  info "Writing configuration to $config"
  declare -a buffer
  declare -a keys_used

  while read -r line; do
    if [[ $line =~ ^([^=]+)\s*=\s*(.*)$ ]]; then
      key="${BASH_REMATCH[1]}"
      keys_used+=("$key")

      if [[ -n "${CONFIG[$key]}" ]]; then
        buffer+=("$key=${CONFIG[$key]}")
      else
        warn "Unable to find key ($key) in read configuration array"
      fi
    else
      buffer+=("$line")
    fi
  done < "$config"

  # add missing keys
  for key in "${!CONFIG[@]}"; do
    # shellcheck disable=SC2076
    if [[ " ${keys_used[*]} " =~ " ${key} " ]]; then
      continue
    else
      warn "Adding new setting ($key) to configuration file"
      buffer+=("$key=${CONFIG[$key]}")
    fi
  done

  printf "%s\n" "${buffer[@]}" > "$config"
}

read_config() {
  config="$_ROOT_PATH/$_CONFIG_FILE"

  info "Reading configuration from $config"
  declare -gA CONFIG=(
    [$CONFIG_PNPM_KEY]="main"
    [$CONFIG_NPM_KEY]="latest"
  )

  if [ ! -f "$config" ]; then
    warn "No configuration found, creating default configuration"
    create_default_config
  fi

  while read -r line; do
    if [[ $line =~ ^([^=]+)\s*=\s*(.*)$ ]]; then
      CONFIG["${BASH_REMATCH[1]}"]="${BASH_REMATCH[2]}"
    fi
  done < "$config"
}

create_default_config() {
  cat > "$_ROOT_PATH/$_CONFIG_FILE" <<EOF
# PNPM Configuration file

# Use sepcified PNPM tag/branch.
# Default: main
$CONFIG_PNPM_KEY=${CONFIG[$CONFIG_PNPM_KEY]}

# Use sepcified NPM tag/branch. Use latest to pick the latest release of NPM
# Default: latest
$CONFIG_NPM_KEY=${CONFIG[$CONFIG_NPM_KEY]}
EOF
}

setup_tmp() {
  rm -rf "${_TMP_PATH:?}"/*
  touch "$_TMP_PATH/.keep"
}
