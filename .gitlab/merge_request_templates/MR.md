<!--
>> Make sure to read the [Contribution Guide](/CONTRIBUTING.md) <<

Provide a brief description of what you fixed. Depending on the scope of the changes you will may need to provide more
or less description. You could use bullet-points of issues fixed:

- description repo#123
- description repo#234

or full textual description (with links to fixes):

Fixed spelling issue with file (fixes repo#123) and wrongfully generated result file (fixes repo#234)
-->
