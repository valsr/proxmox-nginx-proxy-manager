#!/usr/bin/env bash

# shellcheck source=../../common.sh
source "${1-.}/common.sh"

info "Installing pre-requisite packages"
apt-get update
export DEBIAN_FRONTEND=noninteractive
apt-get install -y --no-install-recommends git

if [ ! -d "$_ROOT_PATH" ]; then
  info "Setting up repository to ($_ROOT_PATH)"
  mkdir -p $_ROOT_PATH
  git clone $_REPO_URL $_ROOT_PATH
fi

chmod +u $_ROOT_PATH/update.sh
