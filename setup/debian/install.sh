#!/usr/bin/env bash
set -euo pipefail

# shellcheck source=../../common.sh
source "${1-.}/common.sh"
read_config

WGETOPT=(-t 1 -T 15 -q)
DEVDEPS=(build-essential libffi-dev libssl-dev python3-dev)

clean_install() {
  apt-get remove --purge -y "${DEVDEPS[@]}" -qq &>/dev/null
  apt-get autoremove -y -qq &>/dev/null
  apt-get clean
  rm -rf /root/.cache
}

# add user if missing
if ! id "npm" >/dev/null 2>&1; then
  info "Adding NPM user and group"
  addgroup npm
  adduser -S -G npm npm
fi

# Check for previous install
if [ -f /lib/systemd/system/npm.service ]; then
  info "Stopping services"
  systemctl stop openresty
  systemctl stop npm

  # Cleanup for new install
  info "Cleaning old files"
  rm -rf /app \
  /var/www/html \
  /etc/nginx \
  /var/log/nginx \
  /var/lib/nginx \
  /var/cache/nginx &>/dev/null
fi

# Install dependencies
info "Installing dependencies"
apt-get update
export DEBIAN_FRONTEND=noninteractive
apt-get install -y --no-install-recommends "${DEVDEPS[@]}" gnupg openssl ca-certificates apache2-utils logrotate

# Install Python
info "Installing python"
apt-get install -y -q --no-install-recommends python3 python3-distutils python3-venv
python3 -m venv /opt/certbot/
sed -i 's/include-system-site-packages = false/include-system-site-packages = true/g' /opt/certbot/pyvenv.cfg
export PATH=/opt/certbot/bin:$PATH
grep -qo "/opt/certbot" /etc/environment || echo "$PATH" > /etc/environment
# Install certbot and python dependancies
wget -qO - https://bootstrap.pypa.io/get-pip.py | python -
if [ "$(getconf LONG_BIT)" = "32" ]; then
  pip install --no-cache-dir -U cryptography
fi
pip install --no-cache-dir cffi certbot

# Install openresty
info "Installing openresty"
wget -qO - https://openresty.org/package/pubkey.gpg | apt-key add -
distro_release=$(wget "${WGETOPT[@]}" "http://openresty.org/package/$_DISTRO_ID/dists/" -O - | grep -o "$_DISTRO_CODENAME" | head -n1 || true)
if [ "$_DISTRO_ID" = "ubuntu" ]; then
  echo "deb [trusted=yes] http://openresty.org/package/$_DISTRO_ID ${distro_release:-focal} main" | tee /etc/apt/sources.list.d/openresty.list
else
  echo "deb [trusted=yes] http://openresty.org/package/$_DISTRO_ID ${distro_release:-bullseye} openresty" | tee /etc/apt/sources.list.d/openresty.list
fi
apt-get update && apt-get install -y -q --no-install-recommends openresty

# Install nodejs
info "Installing nodejs"
wget -qO - https://deb.nodesource.com/setup_16.x | bash -
apt-get install -y -q --no-install-recommends nodejs
npm install --global yarn

## figure out which version we need
NPM_VERSION="${CONFIG[$CONFIG_NPM_KEY]}"
if [ "$NPM_VERSION" == "latest" ]; then
  wget "${WGETOPT[@]}" -O ./_latest_release $_NPMURL/releases/latest
  NPM_VERSION="v$(basename "$(grep -wo "expanded_assets/v.*\d" ./_latest_release)" | cut -d'v' -f2)"
fi

info "Checking out NPM version ${NPM_VERSION}"
if [ ! -d "$_ROOT_PATH/npm/.git" ]; then
  info "Downloading NPM repository to $_ROOT_PATH/npm"
  mkdir -p "$_ROOT_PATH/npm"
  git clone "$_NPM_REPO" "$_ROOT_PATH/npm"
fi

cd "$_ROOT_PATH/npm"
git fetch origin --tags
git reset --hard "$NPM_VERSION"

# patch npm
bash "$_ROOT_PATH/patch.sh"

info "Setting up enviroment"
# Crate required symbolic links
ln -sf /usr/bin/python3 /usr/bin/python
ln -sf /opt/certbot/bin/pip /usr/bin/pip
ln -sf /opt/certbot/bin/certbot /usr/bin/certbot
ln -sf /usr/local/openresty/nginx/sbin/nginx /usr/sbin/nginx
ln -sf /usr/local/openresty/nginx/ /etc/nginx

# Update NPM version in package.json files
sed -i "s+0.0.0+$NPM_VERSION+g" backend/package.json
sed -i "s+0.0.0+$NPM_VERSION+g" frontend/package.json

# Fix nginx config files for use with openresty defaults
sed -i 's+^daemon+#daemon+g' docker/rootfs/etc/nginx/nginx.conf
NGINX_CONFS=$(find "$(pwd)" -type f -name "*.conf")
for NGINX_CONF in $NGINX_CONFS; do
  sed -i 's+include conf.d+include /etc/nginx/conf.d+g' "$NGINX_CONF"
done

# Copy runtime files
mkdir -p /var/www/html /etc/nginx/logs
cp -r docker/rootfs/var/www/html/* /var/www/html/
cp -r docker/rootfs/etc/nginx/* /etc/nginx/
cp docker/rootfs/etc/letsencrypt.ini /etc/letsencrypt.ini
cp docker/rootfs/etc/logrotate.d/nginx-proxy-manager /etc/logrotate.d/nginx-proxy-manager
ln -sf /etc/nginx/nginx.conf /etc/nginx/conf/nginx.conf
rm -f /etc/nginx/conf.d/dev.conf

# Create required folders
mkdir -p /tmp/nginx/body \
/run/nginx \
/data/nginx \
/data/custom_ssl \
/data/logs \
/data/access \
/data/nginx/default_host \
/data/nginx/default_www \
/data/nginx/proxy_host \
/data/nginx/redirection_host \
/data/nginx/stream \
/data/nginx/dead_host \
/data/nginx/temp \
/var/lib/nginx/cache/public \
/var/lib/nginx/cache/private \
/var/cache/nginx/proxy_temp

chmod -R 777 /var/cache/nginx
chown root /tmp/nginx

# Dynamically generate resolvers file, if resolver is IPv6, enclose in `[]`
# thanks @tfmm
echo resolver "$(awk 'BEGIN{ORS=" "} $1=="nameserver" {print ($2 ~ ":")? "["$2"]": $2}' /etc/resolv.conf);" > /etc/nginx/conf.d/include/resolvers.conf

# Generate dummy self-signed certificate.
if [ ! -f /data/nginx/dummycert.pem ] || [ ! -f /data/nginx/dummykey.pem ]; then
  info "Generating dummy SSL certificate"
  openssl req -new -newkey rsa:2048 -days 3650 -nodes -x509 -subj "/O=Nginx Proxy Manager/OU=Dummy Certificate/CN=localhost" -keyout /data/nginx/dummykey.pem -out /data/nginx/dummycert.pem
fi

# Copy app files
mkdir -p /app/global /app/frontend/images
cp -r backend/* /app
cp -r global/* /app/global

# Build the frontend
info "Building frontend"
cd ./frontend
export NODE_ENV=development
yarn install --network-timeout=30000
# old version of webpack with hardcoded hash that is deprecated in OpenSSL 3.0
export NODE_OPTIONS=--openssl-legacy-provider
yarn build
cp -r dist/* /app/frontend
cp -r app-images/* /app/frontend/images

# Initialize backend
info "Initializing backend"
rm -rf /app/config/default.json &>/dev/null
if [ ! -f /app/config/production.json ]; then
cat << 'EOF' > /app/config/production.json
{
  "database": {
    "engine": "knex-native",
    "knex": {
      "client": "sqlite3",
      "connection": {
        "filename": "/data/database.sqlite"
      }
    }
  }
}
EOF
fi
cd /app
export NODE_ENV=development
yarn install --network-timeout=30000

# Create NPM service
info "Creating NPM service"
cat << 'EOF' > /lib/systemd/system/npm.service
[Unit]
Description=Nginx Proxy Manager
After=network.target
Wants=openresty.service

[Service]
Type=simple
Environment=NODE_ENV=production
ExecStartPre=-/bin/mkdir -p /tmp/nginx/body /data/letsencrypt-acme-challenge
ExecStart=/usr/bin/node index.js --abort_on_uncaught_exception --max_old_space_size=250
WorkingDirectory=/app
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable npm

# Start services
info "Starting services"
systemctl start openresty
systemctl start npm

IP=$(hostname -I | cut -f1 -d ' ')
info "Installation complete

\e[0mNginx Proxy Manager should be reachable at the following URL.

      http://${IP}:81
"

clean_install
