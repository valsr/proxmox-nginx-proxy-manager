#!/usr/bin/env bash
set -euo pipefail

# shellcheck source=../../common.sh
source "${1-.}/common.sh"
read_config

WGETOPT=(-t 1 -T 15 -q)
DEVDEPS=(npm g++ make gcc python3-dev musl-dev libffi-dev openssl-dev)

clean_install() {
  apk del "${DEVDEPS[@]}" &>/dev/null
}

# add user if missing
if ! id "npm" >/dev/null 2>&1; then
  info "Adding NPM user and group"
  addgroup npm
  adduser -S -G npm npm
fi

# Check for previous install
if [ -f /etc/init.d/npm ]; then
  info "Stopping services"
  rc-service npm stop &>/dev/null
  rc-service openresty stop &>/dev/null
  sleep 2

  info "Cleaning old files"
  # Cleanup for new install
  rm -rf /app \
  /var/www/html \
  /etc/nginx \
  /var/log/nginx \
  /var/lib/nginx \
  /var/cache/nginx &>/dev/null

  info "Removing old dependencies"
  apk del certbot "${DEVDEPS[@]}" &>/dev/null
fi

info "Checking for latest openresty repository"
# shellcheck source=/dev/null
source /etc/os-release
alpine_version=${_DISTRO_VERSION%.*}
# add openresty public key
if [ ! -f /etc/apk/keys/admin@openresty.com-5ea678a6.rsa.pub ]; then
  wget "${WGETOPT[@]}" -P /etc/apk/keys/ http://openresty.org/package/admin@openresty.com-5ea678a6.rsa.pub
fi

# Get the latest openresty repository
repository_version=$(wget "${WGETOPT[@]}" "http://openresty.org/package/alpine/" -O - | grep -Eo "[0-9]{1}\.[0-9]{1,2}" | sort -uVr | head -n1)
repository_version=$(printf "%s\n%s" "$repository_version" "$alpine_version" | sort -V | head -n1)
repository="http://openresty.org/package/alpine/v$repository_version/main"

# Update/Insert openresty repository
grep -q 'openresty.org' /etc/apk/repositories &&
  sed -i "/openresty.org/c\\$repository/" /etc/apk/repositories || echo "$repository" >> /etc/apk/repositories

# Update container OS
info "Updating container OS"
echo "fs.file-max = 65535" > /etc/sysctl.conf
apk update
apk upgrade

# Install dependancies
info "Installing dependencies"
apk add python3 openresty nodejs yarn openssl apache2-utils logrotate bash "${DEVDEPS[@]}"

# Setup python env and PIP
info "Setting up python"
python3 -m venv /opt/certbot/
sed -i 's/include-system-site-packages = false/include-system-site-packages = true/g' /opt/certbot/pyvenv.cfg
python3 -m ensurepip --upgrade
# Install certbot and python dependancies
pip3 install --no-cache-dir -U cryptography
pip3 install --no-cache-dir cffi certbot

## figure out which version we need
NPM_VERSION="${CONFIG[$CONFIG_NPM_KEY]}"
if [ "$NPM_VERSION" == "latest" ]; then
  wget "${WGETOPT[@]}" -O ./_latest_release $_NPMURL/releases/latest
  NPM_VERSION="v$(basename "$(grep -wo "expanded_assets/v.*\d" ./_latest_release)" | cut -d'v' -f2)"
fi

info "Checking out NPM version ${NPM_VERSION}"
if [ ! -d "$_ROOT_PATH/npm/.git" ]; then
  info "Downloading NPM repository to $_ROOT_PATH/npm"
  mkdir -p "$_ROOT_PATH/npm"
  git clone "$_NPM_REPO" "$_ROOT_PATH/npm"
fi

cd $_ROOT_PATH/npm
git fetch origin --tags
git reset --hard "$NPM_VERSION"

bash $_ROOT_PATH/patch.sh "$_ROOT_PATH"

info "Setting up enviroment"
# Crate required symbolic links
ln -sf /usr/bin/python3 /usr/bin/python
ln -sf /usr/bin/pip3 /usr/bin/pip
ln -sf /usr/bin/certbot /opt/certbot/bin/certbot
ln -sf /usr/local/openresty/nginx/sbin/nginx /usr/sbin/nginx
ln -sf /usr/local/openresty/nginx/ /etc/nginx

# Update NPM version in package.json files
sed -i "s+0.0.0+$NPM_VERSION+g" backend/package.json
sed -i "s+0.0.0+$NPM_VERSION+g" frontend/package.json

# Fix nginx config files for use with openresty defaults
sed -i 's+^daemon+#daemon+g' docker/rootfs/etc/nginx/nginx.conf
NGINX_CONFS=$(find "$(pwd)" -type f -name "*.conf")
for NGINX_CONF in $NGINX_CONFS; do
  sed -i 's+include conf.d+include /etc/nginx/conf.d+g' "$NGINX_CONF"
done

# Copy runtime files
mkdir -p /var/www/html /etc/nginx/logs
cp -r docker/rootfs/var/www/html/* /var/www/html/
cp -r docker/rootfs/etc/nginx/* /etc/nginx/
cp docker/rootfs/etc/letsencrypt.ini /etc/letsencrypt.ini
cp docker/rootfs/etc/logrotate.d/nginx-proxy-manager /etc/logrotate.d/nginx-proxy-manager
ln -sf /etc/nginx/nginx.conf /etc/nginx/conf/nginx.conf
rm -f /etc/nginx/conf.d/dev.conf

# Create required folders
mkdir -p /tmp/nginx/body \
/run/nginx \
/data/nginx \
/data/custom_ssl \
/data/logs \
/data/access \
/data/nginx/default_host \
/data/nginx/default_www \
/data/nginx/proxy_host \
/data/nginx/redirection_host \
/data/nginx/stream \
/data/nginx/dead_host \
/data/nginx/temp \
/var/lib/nginx/cache/public \
/var/lib/nginx/cache/private \
/var/cache/nginx/proxy_temp

chmod -R 777 /var/cache/nginx
chown root /tmp/nginx

# Dynamically generate resolvers file, if resolver is IPv6, enclose in `[]`
# thanks @tfmm
echo resolver "$(awk 'BEGIN{ORS=" "} $1=="nameserver" {print ($2 ~ ":")? "["$2"]": $2}' /etc/resolv.conf);" > /etc/nginx/conf.d/include/resolvers.conf

# Generate dummy self-signed certificate.
if [ ! -f /data/nginx/dummycert.pem ] || [ ! -f /data/nginx/dummykey.pem ]; then
  info "Generating dummy SSL certificate"
  openssl req -new -newkey rsa:2048 -days 3650 -nodes -x509 -subj "/O=Nginx Proxy Manager/OU=Dummy Certificate/CN=localhost" -keyout /data/nginx/dummykey.pem -out /data/nginx/dummycert.pem
fi

# Copy app files
mkdir -p /app/global /app/frontend/images
cp -r backend/* /app
cp -r global/* /app/global

# Build the frontend
info "Building frontend"
cd ./frontend
export NODE_ENV=development
yarn install
# old version of webpack with hardcoded hash that is deprecated in OpenSSL 3.0
export NODE_OPTIONS=--openssl-legacy-provider
yarn build
cp -r dist/* /app/frontend
cp -r app-images/* /app/frontend/images

# Initialize backend
info "Initializing backend"
rm -rf /app/config/default.json &>/dev/null
if [ ! -f /app/config/production.json ]; then
cat << 'EOF' > /app/config/production.json
{
  "database": {
    "engine": "knex-native",
    "knex": {
      "client": "sqlite3",
      "connection": {
        "filename": "/data/database.sqlite"
      }
    }
  }
}
EOF
fi
cd /app
export NODE_ENV=development
yarn install

# Create NPM service
info "Creating NPM service"
cat << 'EOF' > /etc/init.d/npm
#!/sbin/openrc-run
description="Nginx Proxy Manager"

command="/usr/bin/node"
command_args="index.js --abort_on_uncaught_exception --max_old_space_size=250"
command_background="yes"
directory="/app"

pidfile="/var/npm.pid"
output_log="/var/log/npm.log"
error_log="/var/log/npm.err"

depends () {
  before openresty
}

start_pre() {
  mkdir -p /tmp/nginx/body \
  /data/letsencrypt-acme-challenge

  export NODE_ENV=production
}

stop() {
  pkill -9 -f node
  return 0
}

restart() {
  $0 stop
  $0 start
}
EOF
chmod a+x /etc/init.d/npm
rc-update add npm boot &>/dev/null
rc-update add openresty boot &>/dev/null
rc-service openresty stop &>/dev/null

# Start services
info "Starting services"
rc-service openresty start
rc-service npm start

IP=$(ip a s dev eth0 | sed -n '/inet / s/\// /p' | awk '{print $2}')
info "Installation complete

\e[0mNginx Proxy Manager should be reachable at the following URL.

      http://${IP}:81
"

clean_install
