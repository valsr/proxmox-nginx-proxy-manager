#!/usr/bin/env bash
set -Eeuo pipefail
trap do_error ERR SIGTERM

_PWD=$(pwd)

source common.sh
setup_tmp

DISTRO=$_DISTRO_ID
if [ "$DISTRO" = "ubuntu" ]; then
  DISTRO="debian"
fi

# Config variables
PNPM_VERSION=""

# User supplied argumets
_PNPM_VERSION=""
_NPM_VERSION=""


function help(){
  info "Update PNPM"
  info ""
  info "Usage: ./update.sh [OPTIONS]"
  info ""
  info "Options:"
  info "--help          Show this error message"
  info "--pnpm=version  Update to this branch/tag (will remember for next time)"
  info "--npm=version   Update npt to this branch/tag (will remember for next time). Use latest to update to the latest\
  NPM release"
}

function run_update(){
  # handle user input
  original_args="$*"
  while [[ $# -gt 0 ]]; do
    arg="$1"

    case $arg in
      --pnpm)
        _PNPM_VERSION=$2
        shift
        ;;
      --npm)
        _NPM_VERSION=$2
        shift
        ;;
      --help)
        help
        exit 0
        ;;
      *)
        error "Unrecognized option '$1'"
        exit 2
        ;;
    esac
    shift
  done

  # shellcheck disable=SC2119
  read_config

  # handle user overwrites
  if [ -n "$_PNPM_VERSION" ]; then
    CONFIG[$CONFIG_PNPM_KEY]=$_PNPM_VERSION
  fi

  PNPM_VERSION="${CONFIG[$CONFIG_PNPM_KEY]}"

  if [ -n "$_NPM_VERSION" ]; then
    CONFIG[$CONFIG_NPM_KEY]=$_NPM_VERSION
  fi

  # shellcheck disable=SC2119
  save_config

  info "Updating PNPM repo to $PNPM_VERSION"
  version=$(md5sum $_ROOT_PATH/update.sh)
  git fetch --prune --prune-tags --tags --progress origin
  git reset --hard "origin/$PNPM_VERSION"
  new_version=$(md5sum $_ROOT_PATH/update.sh)
  chmod +x $_ROOT_PATH/update.sh

  if [ "$version" != "$new_version" ]; then
    warn "Update script updated - re-running with arguments: $original_args"
    if [ "" != "${original_args[*]}" ]; then
      bash "$_ROOT_PATH/update.sh" "${original_args[*]}"
    else
      bash "$_ROOT_PATH/update.sh"
    fi
  fi

  info "Updating NPM installation"
  bash "$_ROOT_PATH/setup/$DISTRO/install.sh"
  cd "$_PWD"
}

if [ "$EUID" -ne 0 ]; then
  error "Must run as root user"
  exit 2
fi

run_update "$@" > >(tee -a "$_LOG_FILE") 2> >(tee -a "$_LOG_FILE" >&2)
