# Proxmox Nginx Proxy Manager

> 📝 NOTE: This project is in early version - things may not work or break after each update!

This project aims to simplify installation/setup of [Nginx Proxy Manager](https://github.com/NginxProxyManager/nginx-proxy-manager) as a Proxmox LXC Container. Based on installation scripts of [ej52](https://github.com/ej52/proxmox-scripts/).

## Setup instructions

1. Download and install alpine templates in proxmox

  ```bash
  pveam download <template-storage-location> alpine-3.17-default_20221129_amd64.tar.xz
  ```

2. Install the reverse proxy by running

  ```bash
  curl -sL https://gitlab.com/valsr/proxmox-nginx-proxy-manager/-/raw/main/create.sh | bash -s
  ```

### Command line arguments

| argument    | default             | description                         |
| ----------- | ------------------- | ----------------------------------- |
| --id        | $nextid             | container id                        |
| --bridge    | vmbr0               | bridge used for eth0                |
| --cores     | 1                   | number of cpu cores                 |
| --disksize  | 2G                  | size of disk                        |
| --hostname  | nginx-proxy-manager | hostname of the container           |
| --memory    | 512                 | amount of memory                    |
| --storage   | local-lvm           | storage location for container disk |
| --templates | local               | storage location for templates      |
| --swap      | 0                   | amount of SWAP                      |

you can set these parameters by appending ` -- <parameter> <value>` like:

```bash
curl -sL https://gitlab.com/valsr/proxmox-nginx-proxy-manager/-/raw/main/create.sh | bash -s -- --cores 4
```

### Console

There is no login required to access the console from the Proxmox web UI. If you are presented with a blank screen, press `CTRL + C` to generate a prompt.

> 📝 NOTE: If you have installed this using the alternative usage, a password may be required!

## Alternative usage

If you are not using proxmox or want to install this on a existing Alpine/Debian box, you can run the setup script
itself.

***Note:*** _Only Alpine, Debian and Ubuntu are currently supported by this script_

```bash
wget --no-cache -qO - https://gitlab.com/valsr/proxmox-nginx-proxy-manager/-/raw/main/setup.sh | sh
```

## Updating installation

An update script is available to update the installation - both setup script and nginx proxy manager. To update run
the update script from the PNPM repo folder (this is usually */opt/proxmox-nginx-proxy-manager*):

```bash
/opt/proxmox-nginx-proxy-manager/update.sh
```

This will update the repository (fetch latest version) as well as update the NPM installation. You can optionally
specify which branch/tag update PNPM and which version to update NPM to. These settings will be remembered for next
time (stored in .pnpm config file).

If for any reason the updater fails, you can manually pull the changes to and re-running the udpate
script.

```bash
cd /opt/proxmox-nginx-proxy-manager
git reset --hard HEAD
# use main or anyother branch/tag as you need
git pull origin main
bash ./update.sh
```

### Command line arguments

| argument | default | description                                                                            |
| -------- | ------- | -------------------------------------------------------------------------------------- |
| --pnpm   | main    | Branch/tag for updating PNPM.                                                          |
| --npm    | latest  | Branch/tag/release to update NPM to. latest will update to the latest release instead. |

## Getting support

- [Changelog](CHANGELOG.MD)
- [Found a bug?](https://gitlab.com/valsr/proxmox-nginx-proxy-manager/-/issues)
- [Contributing](CONTRIBUTING.MD)
