#!/usr/bin/env bash
# Patches NPM. Patches are selected from several locations from the patch directory (patches) - see folrder structure
# below). Note patches with same name will overwritten based on the following order:
# - common
# - npm_version
# - distro
# - distro version
#
# Patch folder structure:
# PATCHDIR/common/: common patches applying to all distros
# PATCHDIR/npm_<major>/: patches applying to all distros for npm <major> - i.e. PATCHDIR/dist_npm_2
# PATCHDIR/dist_<distro>/: patches applying to <distro> distro - i.e. PATCHDIR/dist_alpine
# PATCHDIR/dist_<distro>_<ver>/: patches applying to <distro> distro and selected distro version - i.e. PATCHDIR/dist_alpine_3.7

set -euo pipefail

ROOT_PATH="${1:=.}"

# shellcheck source=common.sh
source "$ROOT_PATH/common.sh"
read_config "$_ROOT_PATH"

PATCH_PATH="$_TMP_PATH/patches"
mkdir -p "$PATCH_PATH"

# copy common patches
if [ -d "$_ROOT_PATH/$_PATCH_DIR/common" ]; then
  info "Copying common patches"
  cp --remove-destination "$_ROOT_PATH/$_PATCH_DIR/common"/* "$PATCH_PATH"
fi

# copy npm patches
npm_version=$(cut -f 1 -d. < "$_ROOT_PATH/$_NPM_DIR/.version")
if [ -d "$_ROOT_PATH/$_PATCH_DIR/npm_${npm_version}" ]; then
  info "Copying NPM $npm_version patches"
  cp --remove-destination "$_ROOT_PATH/$_PATCH_DIR/npm_${npm_version}"/* "$PATCH_PATH"
fi

# copy distro patches
if [ -d "$_ROOT_PATH/$_PATCH_DIR/dist_${_DISTRO_ID}" ]; then
  info "Copying distro $_DISTRO_ID patches"
  cp --remove-destination "$_ROOT_PATH/$_PATCH_DIR/dist_${_DISTRO_ID}"/* "$PATCH_PATH"
fi

# copy distro version patches
if [ -d "$_ROOT_PATH/$_PATCH_DIR/dist_${_DISTRO_ID}_${_DISTRO_MAJOR_VERSION}" ]; then
  info "Copying distro $_DISTRO_ID:$_DISTRO_MAJOR_VERSION patches"
  cp --remove-destination "$_ROOT_PATH/$_PATCH_DIR/dist_${_DISTRO_ID}_${_DISTRO_MAJOR_VERSION}"/* "$PATCH_PATH"
fi

# execute paches in order
PWD="$(pwd)"
cd "$_ROOT_PATH/$_NPM_DIR"
for f in "$PATCH_PATH"/*; do
  if [ -f "$f" ]; then
    info "Applying patch $f"
    git am "$f"
  fi
done

cd "$PWD"
