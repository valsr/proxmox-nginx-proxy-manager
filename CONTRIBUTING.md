# How to contribute

[< back to readme](README.md)

You want to contribute? Awesome. Lets get through the gritty details so that we can get your code home! This guide
should help you at least a little bit in getting there, you might need to add some elbow-grease and some brain as well.

## TOC

1. [Getting Started](#getting-started)
1. [Making Changes](#making-changes)
1. [Testing Changes](#testing-changes)
1. [Submitting Changes](#submitting-changes)
1. [Get Help](#get-help)

## Getting Started

Submitting changes(code) follows a simple process can be summarized in the following diagram:

```plantuml
@startuml
"fork/branch"->commit
commit->test
test->pull request
@enduml
```

Your first step will be to fork the repository, which means you will need a [gitlab account](https://gitlab.com/). Once
you fork the repository you will need to can begin making your changes.

## Making Changes

1. Create a topic branch from where you want to base your work
  - This is usually the **master** branch
  - Only target **release branches** if you are certain your fix must be on that branch
1. Make commits of *logical* and *atomic* units
1. Make sure your commit messages are in the proper format
1. Make sure you have added the necessary tests for your changes

### Making Trivial Changes

For trivial changes (documentation updates, typo corrections, any small non-code changes) you can omit most of the
requirements - just fork/branch-commit-pull request will suffice. If the change is not a minor one, then your pull
request will be rejected and you will need to resubmit using the standard process.

## Testing Changes

As of right now the only requirement is that you manually test your change and that it doesn't break anything while you
do your due diligence. In the future this requirement will hopefully change.

## Submitting Changes

Once your changes are ready to go you will need to do the following:

1. rebase your changes on top of the most recent version of the branch you want to merge to
1. verify your changes are okay:
  - make sure they are well documented
  - make sure you have tested them locally
  - make sure the project builds and installs/updates properly
1. create a pull request and follow the request template
1. once your changes have been reviewed you might need to perform corrections based on the feedback from the PR

If any of these stages fail, you will need to amend your PR to address the issues found. To see what these stages do you
can check the [gitlab CI configuration](.gitlab-ci.yml).

## Get Help

If you need more help with any aspect of the project or how to submit your work you can ask a `question` to the issues
page!
